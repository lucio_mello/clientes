unit uClientes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, uCliente, uVendedor,
  System.Generics.Collections, Vcl.Grids, Vcl.Buttons;

type
  TForm1 = class(TForm)
    bt1: TButton;
    StringGrid1: TStringGrid;
    novo: TButton;
    Button1: TButton;
    Button2: TButton;
    Fechar: TButton;
    procedure atualizarGrid;
    procedure buscarClientes;
    procedure buscarVendedor;
    procedure bt1Click(Sender: TObject);
    procedure novoClick(Sender: TObject);
    procedure StringGrid1DrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure ClickBotaoEditar(Sender: TObject);
    procedure ClickBotaoExcluir(Sender: Tobject);
    procedure atualizarDados;
    procedure FormCreate(Sender: TObject);
    procedure FecharClick(Sender: TObject);
  private
    { Private declarations }
    cliente: TCliente;
    clientes: TObjectList<TCliente>;
    vendedor: TVendedor;
    vendedores: TObjectList<TVendedor>;
    retornoClientes: TList;
    retornoVendedor: TList;
    function buscarVendedorPorId(id: integer): TVendedor;
    function buscarClientePorId(id: integer): TCliente;
    function criarBotao(caption: String; ACol, ARow: Integer): TBitBtn;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses
  EditarClientes;

function consultarClientes(): TList; stdcall; external 'dllCon.dll';
function consultarVendedores(): TList; stdcall; external 'dllCon.dll';
function excluirCliente(cliente: TCliente): Boolean; stdcall; external 'dllCon.dll';

procedure TForm1.bt1Click(Sender: TObject);
begin
  atualizarDados;
  buscarClientes;
  buscarVendedor;
  atualizarGrid;
end;

procedure TForm1.buscarClientes;
var
  i: integer;
begin
  clientes := TObjectList<TCliente>.Create;
  for i := 0 to retornoClientes.Count - 1 do
  begin
    cliente := TCliente.Create;
    cliente.FTCLI_CLIENTE_PK := TCliente(retornoClientes[i]).FTCLI_CLIENTE_PK;
    cliente.FTCLI_RAZAO_SOCIAL := pAnsiChar(TCliente(retornoClientes[i]).FTCLI_RAZAO_SOCIAL);
    cliente.FTCLI_VENDENDOR_FK := TCliente(retornoClientes[i]).FTCLI_VENDENDOR_FK;
    clientes.Add(cliente);
  end;
end;

procedure TForm1.buscarVendedor;
var
  i: integer;
begin
  vendedores := TObjectList<TVendedor>.Create;
  for i := 0 to retornoVendedor.Count - 1 do
  begin
    vendedor := TVendedor.Create;
    vendedor.FTVEN_VENDEDOR_PK := TVendedor(retornoVendedor[i]).FTVEN_VENDEDOR_PK;
    vendedor.FTVEN_NOME := pAnsiChar(TVendedor(retornoVendedor[i]).FTVEN_NOME);
    vendedores.Add(vendedor);
  end;
end;

procedure TForm1.atualizarGrid;
var
  i: integer;
begin
  {Limpando o StringGrid}
  for i := 0 to StringGrid1.RowCount - 1 do
    StringGrid1.Rows[i].Clear;

  {Define o numero de linhas e colunas do StringGrid}
  StringGrid1.RowCount := clientes.Count + 1;
  StringGrid1.ColCount := 6;

  {T�tulos das colunas}
  StringGrid1.Cells[0, 0] := 'Id Cliente';
  StringGrid1.Cells[1, 0] := 'Nome Cliente';
  StringGrid1.Cells[2, 0] := 'Id Vendedor';
  StringGrid1.Cells[3, 0] := 'Nome Vendedor';
  StringGrid1.ColWidths[0] := 80;
  StringGrid1.ColWidths[1] := 250;
  StringGrid1.ColWidths[2] := 80;
  StringGrid1.ColWidths[3] := 250;
  {Preenchendo o grid}
  for i := 0 to clientes.Count - 1 do
  begin
    StringGrid1.Cells[0, i + 1] := clientes[i].FTCLI_CLIENTE_PK.ToString;
    StringGrid1.Cells[1, i + 1] := clientes[i].FTCLI_RAZAO_SOCIAL;
    StringGrid1.Cells[2, i + 1] := clientes[i].FTCLI_VENDENDOR_FK.ToString;
    vendedor := buscarVendedorPorId(clientes[i].FTCLI_VENDENDOR_FK);
    StringGrid1.Cells[3, i + 1] := vendedor.FTVEN_NOME;
  end;
end;

function TForm1.buscarVendedorPorId(id: integer): TVendedor;
var
  i: integer;
begin
  for i := 0 to vendedores.Count - 1 do
  begin
    if (id = vendedores[i].FTVEN_VENDEDOR_PK) then
    begin
      Result := vendedores[i];
    end;
  end;
end;

function TForm1.buscarClientePorId(id: integer): TCliente;
var
  i: integer;
begin
  for i := 0 to clientes.Count - 1 do
  begin
    if (id = clientes[i].FTCLI_CLIENTE_PK) then
    begin
      Result := clientes[i];
    end;
  end;
end;

procedure TForm1.novoClick(Sender: TObject);
begin
  if Form2 = nil then
    Application.CreateForm(TForm2, Form2);
  Form2.ShowModal;
  atualizarDados;
  buscarClientes;
  buscarVendedor;
  atualizarGrid;
end;

procedure TForm1.StringGrid1DrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  R: TRect;
  button: TBitBtn;
begin

  if ((ACol = 4) AND (ARow <> 0)) then
  begin
    button := criarBotao('Editar', ACol, ARow);
    button.OnClick := ClickBotaoEditar;
  end;

  if ((ACol = 5) AND (ARow <> 0)) then
  begin
    button := criarBotao('Excluir', ACol, ARow);
    button.OnClick := ClickBotaoExcluir;
  end;

end;

function TForm1.criarBotao(caption: String; ACol, ARow: Integer): TBitBtn;
var
 button: TBitBtn;
 R: TRect;
begin
    button := TBitBtn.Create(Self);
    button.Caption := caption;
    button.Parent := Form1;
    button.Visible := False;
    Result := button;
    R := StringGrid1.CellRect(ACol, ARow);
    R.Left := R.Left + StringGrid1.Left;
    R.Right := R.Right + StringGrid1.Left;
    R.Top := R.Top + StringGrid1.Top;
    R.Bottom := R.Bottom + StringGrid1.Top;
    button.Caption := caption;
    button.Left := R.Left + 1;
    button.Top := R.Top + 1;
    button.Width := (R.Right + 1) - R.Left;
    button.Height := (R.Bottom + 1) - R.Top;
    button.Visible := True;
    button.Tag := ARow;
end;

procedure TForm1.ClickBotaoEditar(Sender: Tobject);
var
  ARow, id: Integer;
begin
  ARow := TBitBtn(Sender).Tag;
  id := StrTOInt(StringGrid1.Cells[0, ARow]);
  cliente := TCliente.Create;
  cliente := buscarClientePorId(id);
  if Form2 = nil then
  begin
    Application.CreateForm(TForm2, Form2);
    Form2.codCliente.Text := IntToStr(cliente.FTCLI_CLIENTE_PK);
    Form2.razaoSocai.Text := cliente.FTCLI_RAZAO_SOCIAL;
    Form2.vId.Text := IntToStr(cliente.FTCLI_VENDENDOR_FK);
    vendedor := buscarVendedorPorId(cliente.FTCLI_VENDENDOR_FK);
    Form2.vNome.Text := vendedor.FTVEN_NOME;
  end;
  Form2.ShowModal;
  atualizarDados;
  buscarClientes;
  buscarVendedor;
  atualizarGrid;
end;

procedure TForm1.ClickBotaoExcluir(Sender: Tobject);
var
  ARow, id: Integer;
  ret: boolean;
begin
  ARow := TBitBtn(Sender).Tag;
  id := StrTOInt(StringGrid1.Cells[0, ARow]);
  cliente := TCliente.Create;
  cliente := buscarClientePorId(id);
  if Application.MessageBox('Tem certeza que deseja excluir?', 'ATEN��O',
      MB_ICONHAND + MB_YESNO + MB_SYSTEMMODAL) = 6  then
  begin
    excluirCliente(cliente);
  //  atualizarDados;
  //  buscarClientes;
  //  buscarVendedor;
  //  atualizarGrid;
    showMessage('Registro exclu�do com sucesso');
  end;
end;

procedure TForm1.FecharClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  atualizarDados;
end;

procedure TForm1.atualizarDados;
begin
  retornoClientes := consultarClientes;
  retornoVendedor := consultarVendedores;
end;

end.
