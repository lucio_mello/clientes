object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 371
  ClientWidth = 877
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object bt1: TButton
    Left = 24
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Listar Todos'
    TabOrder = 0
    OnClick = bt1Click
  end
  object StringGrid1: TStringGrid
    Left = 24
    Top = 39
    Width = 833
    Height = 314
    ColCount = 2
    RowCount = 2
    Options = [goFixedHorzLine, goVertLine, goHorzLine, goRowMoving, goColMoving]
    TabOrder = 1
    OnDrawCell = StringGrid1DrawCell
  end
  object novo: TButton
    Left = 104
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Novo Cliente'
    TabOrder = 2
    OnClick = novoClick
  end
  object Button1: TButton
    Left = 184
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Excluir'
    TabOrder = 3
  end
  object Button2: TButton
    Left = 265
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Editar'
    TabOrder = 4
  end
  object Fechar: TButton
    Left = 782
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Fechar'
    TabOrder = 5
    OnClick = FecharClick
  end
end
