unit EditarClientes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, uCliente;

type
  TForm2 = class(TForm)
    razaoSocai: TLabeledEdit;
    vNome: TEdit;
    Label1: TLabel;
    vId: TEdit;
    Button1: TButton;
    Salvar: TButton;
    codCliente: TEdit;
    Label2: TLabel;
    procedure gravar;
    procedure SalvarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
     cliente : TCliente;
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

function salvarCliente(cliente: TCliente): Boolean; stdcall; external 'dllCon.dll';
function editarCliente(cliente: TCliente): Boolean; stdcall; external 'dllCon.dll';

procedure TForm2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
  Form2 := nil;
end;

procedure TForm2.gravar;
var
 ret: Boolean;
 razao: AnsiString;
begin
  if vId.Text = '' then
    ShowMessage('O campo vendedor � obrigat�rio')
  else if razaoSocai.Text = '' then
    ShowMessage('O campo Raz�o Social � obrigat�rio')
  else
    begin
      cliente := TCliente.Create;
      razao := AnsiString(razaoSocai.text);
      cliente.FTCLI_RAZAO_SOCIAL := razao;
      cliente.FTCLI_VENDENDOR_FK := StrToInt(vId.Text);
      if cliente.FTCLI_CLIENTE_PK = 0 then
         ret := salvarCliente(cliente)
      else
        begin
          cliente.FTCLI_CLIENTE_PK := StrToInt(codCliente.Text);
          ret := editarCliente(cliente);
        end;

      if ret then
        ShowMessage('Registro Atualizado com sucesso')
      else
        ShowMessage('erro... :(');

      Close;
    end;
end;

procedure TForm2.SalvarClick(Sender: TObject);
begin
  gravar;
end;

end.
