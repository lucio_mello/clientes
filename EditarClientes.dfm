object Form2: TForm2
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Editar Cliente'
  ClientHeight = 194
  ClientWidth = 426
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 40
    Top = 85
    Width = 31
    Height = 13
    Caption = 'Label1'
  end
  object Label2: TLabel
    Left = 247
    Top = 11
    Width = 59
    Height = 13
    Caption = 'C'#243'd. Ciiente'
  end
  object razaoSocai: TLabeledEdit
    Left = 40
    Top = 48
    Width = 345
    Height = 21
    EditLabel.Width = 60
    EditLabel.Height = 13
    EditLabel.Caption = 'Raz'#227'o Social'
    TabOrder = 0
  end
  object vNome: TEdit
    Left = 77
    Top = 104
    Width = 244
    Height = 21
    TabOrder = 1
  end
  object vId: TEdit
    Left = 40
    Top = 104
    Width = 31
    Height = 21
    TabOrder = 2
  end
  object Button1: TButton
    Left = 327
    Top = 102
    Width = 58
    Height = 25
    Caption = 'Pesquisar'
    TabOrder = 3
  end
  object Salvar: TButton
    Left = 168
    Top = 144
    Width = 75
    Height = 25
    Caption = 'Salvar'
    TabOrder = 4
    OnClick = SalvarClick
  end
  object codCliente: TEdit
    Left = 312
    Top = 8
    Width = 41
    Height = 21
    ReadOnly = True
    TabOrder = 5
  end
end
